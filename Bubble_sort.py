def bubble_sort(arr, verbose = 0):
    swaps = 0
    for i in range(0,len(arr)-1):
        if arr[i] > arr[i+1]:
            arr[i], arr[i + 1] = arr[i+1], arr[i]
            swaps +=1
    if verbose:
        print(f"swaps: {swaps}")
        print(arr)
    if swaps == 0:
        return arr
    else:
        return bubble_sort(arr, verbose=1)

l = [2,1,4,6,8,3,0,1]
bubble_sort(l, 1)