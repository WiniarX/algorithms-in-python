class BinarySearch():
    def __init__(self, list_for_search, initial_idx=0):
        self.list = list_for_search
        self.last_idx = len(list_for_search) - 1
        self.initial_idx = initial_idx

    def search(self, x):
        # print(f"x : {x}")
        idx = (self.initial_idx + self.last_idx) // 2
        # print(f"idx = {idx}")
        discovery = self.list[idx]
        # print(f"discover = {discovery}")
        if self.initial_idx > self.last_idx:
            return None
        # print(discovery == x)
        if discovery == x:
            return idx
        if discovery > x:
            self.last_idx = idx - 1
            return self.search(x)
        if discovery < x:
            self.initial_idx = idx + 1
            return self.search(x)