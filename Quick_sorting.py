def quicksort(lista: list):
    if len(lista) > 1:
        pivot = lista[-1]
        lower = [i for i in lista[:-1] if i <= pivot]
        higher = [i for i in lista[:-1] if i > pivot]
        return quicksort(lower)+[pivot]+ quicksort(higher)
    else:
        return lista


l = [3,5,1,2,9,6,3,13]
print(quicksort(l))
