class DoubleLinkedList():
    def __init__(self, head=None):
        """Creator
        Create list with initial node
        :param head - Initial node
        """
        if head is None:
            self.head = None
        else:
            if type(head) is self.Node:
                self.head = head
                self.head.prev = None
            else:
                node = self.Node(head)
                self.head = node

    def last_node(self, actual_node=None):
        """ Last Node Finder
        func that recursively find last node in list
        """
        if self.head is None:  # check if head is None, if not return nothing
            return None
        elif actual_node is None:  # set starting node as head node if nothing given
            actual_node = self.head
        next_node = actual_node.next  # set next Node to emory
        if next_node is None:  # check if actual node is the last
            return actual_node
        else:
            return self.last_node(next_node)  # if recursively make the same func on the next node

    def append(self, node):
        """Append
        func that append (put on the last place) given node"""

        if type(node) is self.Node:  # chceck if we append Node if not create Node with Node.val = node
            appending_node = node
        else:
            appending_node = self.Node(node)
        if self.head is None:  # If there is no head, set one
            appending_node.prev = None
            appending_node.next = None
            self.head = appending_node
            return
        last_node = self.last_node()  # Find last Node
        last_node.next = appending_node  # Set pointer from last node to appending node
        appending_node.prev = last_node  # Set pointer from appending node to last node
        appending_node.next = None  # Set appending node as last one
        return

    def prepend(self, node):
        """Prepend
        func that prepend (put on the first place) given node"""
        if type(node) is self.Node:  # chceck if we prepend Node if not create Node with Node.val = node
            prepending_node = node
        else:
            prepending_node = self.Node(node)
        if self.head is None:
            prepending_node.prev = None
            prepending_node.next = None
            self.head = prepending_node
            return
        self.head.prev = prepending_node  # Find last Node
        prepending_node.prev = None  # Set pointer from last node to prepending node
        prepending_node.next = self.head  # Set pointer from prepending node to last node
        self.head = prepending_node  # Set prepending node as last one
        return

    def show_all(self, node=None):
        """Pritning func
        Function that print values from nodes from actual list"""
        if node is None:
            node = self.head
        next = node.next
        print(node.val)
        if next is None:
            return None
        else:
            return self.show_all(next)

    def remove(self, node):
        """Remover
        remove node from the list"""
        node.val = None
        node.prev = None
        node.next = None

    def del_by_val(self, val, node=None):
        """Deleter
        Func that delete node used self.remove() function if node.val == val"""
        if node is None:
            if self.head is None:
                return
            testing_node = self.head
        else:
            testing_node = node
        next_node = testing_node.next
        prev_node = testing_node.prev
        if testing_node.val == val:
            if testing_node is self.head:
                if next_node is None:
                    self.remove(testing_node)
                    self.head = None
                    return
                else:
                    next_node.prev = None
                    self.head = next_node
                    self.remove(testing_node)
                    return self.del_by_val(val=val, node=next_node)
            if next_node is None:
                prev_node.next = None
                self.remove(testing_node)
                return
            else:
                prev_node.next = next_node
                next_node.prev = prev_node
                self.remove(testing_node)
                return self.del_by_val(val=val, node=next_node)
        else:
            if next_node is None:
                return
            return self.del_by_val(val=val, node=next_node)

    class Node():
        def __init__(self, val):
            self.val = val
            self.next = None
            self.prev = None

### TEST ###
# list = DoubleLinkedList()
# node = DoubleLinkedList().Node(6)
# node2 = DoubleLinkedList().Node(7)
# node3 = [1,2,3,4,5]
# list.prepend(node)
# list.append(node)
# list.prepend(node2)
# list.append(node2)
# list.prepend(node3)
# list.append(node3)
# list.append(7)
# list.append(node)
# list.show_all()
# list.del_by_val(val=node.val)
# print()
# list.show_all()
